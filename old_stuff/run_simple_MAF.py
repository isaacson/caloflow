"""
CaloFlow: Normalizing Flows for Calorimeter Shower generation
code based on code from https://github.com/kamenbliznashki/normalizing_flows

"""

import argparse
import os
import math

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.distributions as D
#from torchvision.utils import save_image
import plot_calo
import numpy as np

from data import get_dataloader

torch.set_default_dtype(torch.float64)

parser = argparse.ArgumentParser()

parser.add_argument('--train', action='store_true', help='train a flow')
parser.add_argument('--generate', action='store_true', help='generate from a trained flow')
parser.add_argument('--play', action='store_true', help='for playing around')
parser.add_argument('--restore_file', type=str, help='Path to model to restore.')
parser.add_argument('--plot_true', action='store_true', help='plot a random batch of train data ordered by energy')
parser.add_argument('--plot_true_avg', action='store_true', help='plot average of all training data')
parser.add_argument('--no_cuda', action='store_true', help='Do not use cuda.')

parser.add_argument('--particle_type', '-p',
                    help='Which particle to shower, gamma, eplus, or piplus')
parser.add_argument('--num_layer', default=0,
                    help='How many Calolayers are trained')
parser.add_argument('--output_dir', default='./results', help='Where to store the output')
parser.add_argument('--data_dir', default='/media/claudius/8491-9E93/ML_sources/CaloGAN')
parser.add_argument('--results_file', default='results.txt',
                    help='Filename where to store settings and test results.')
parser.add_argument('--use_full', '-u', action='store_true',
                    help='Train on full dataset, no test set')

parser.add_argument('--energy_encoding', default='direct',
                    help='How the energy conditional is given to the NN: "direct", '+\
                    '"one_hot" or "one_blob"')
parser.add_argument('--energy_encoding_bins', default=100,
                    help='Number of bins in one_hot or one_blob energy encoding')

# MAF parameters
parser.add_argument('--n_blocks', type=int, default=5,
                    help='Number of blocks to stack in a model (MADE in MAF).')
parser.add_argument('--hidden_size', type=int, default=100,
                    help='Hidden layer size for each MADE block in an MAF.')
parser.add_argument('--n_hidden', type=int, default=1,
                    help='Number of hidden layers in each MADE.')
parser.add_argument('--activation_fn', type=str, default='relu',
                    help='What activation function to use in the MADEs.')
parser.add_argument('--input_order', type=str, default='sequential',
                    help='What input order to use (sequential | random).')
parser.add_argument('--no_batch_norm', action='store_true', help='Disable batch normalization')

# training params
parser.add_argument('--batch_size', type=int, default=50)
parser.add_argument('--n_epochs', type=int, default=20)
parser.add_argument('--lr', type=float, default=1e-5, help='Learning rate.')
parser.add_argument('--log_interval', type=int, default=1000,
                    help='How often to show loss statistics and save samples.')
parser.add_argument('--start_epoch', default=0,
                    help='Starting epoch (for logging; to be overwritten when restoring file.')

def create_masks(input_size, hidden_size, n_hidden, input_order='sequential', input_degrees=None):
    """ MADE paper sec 4:
        degrees of connections between layers -- ensure at most in_degree - 1 connections
    """
    degrees = []

    # set input degrees to what is provided in args
    # (the flipped order of the previous layer in a stack of mades);
    # else init input degrees based on strategy in input_order (sequential or random)
    if input_order == 'sequential':
        degrees += [torch.arange(input_size)] if input_degrees is None else [input_degrees]
        for _ in range(n_hidden + 1):
            degrees += [torch.arange(hidden_size) % (input_size - 1)]
        degrees += [torch.arange(input_size) % input_size - 1] if input_degrees is None\
            else [input_degrees % input_size - 1]

    elif input_order == 'random':
        degrees += [torch.randperm(input_size)] if input_degrees is None else [input_degrees]
        for _ in range(n_hidden + 1):
            min_prev_degree = min(degrees[-1].min().item(), input_size - 1)
            degrees += [torch.randint(min_prev_degree, input_size, (hidden_size,))] # suspect bug: input_size-1
        min_prev_degree = min(degrees[-1].min().item(), input_size - 1)
        degrees += [torch.randint(min_prev_degree, input_size, (input_size,)) - 1]\
            if input_degrees is None else [input_degrees - 1]

    # construct masks
    masks = []
    for (d0, d1) in zip(degrees[:-1], degrees[1:]):
        masks += [(d1.unsqueeze(-1) >= d0.unsqueeze(0)).double()]

    return masks, degrees[0]

class MaskedLinear(nn.Linear):
    """ MADE building block layer """
    def __init__(self, input_size, n_outputs, mask, cond_label_size=None):
        super().__init__(input_size, n_outputs)

        self.register_buffer('mask', mask)

        self.cond_label_size = cond_label_size
        if cond_label_size is not None:
            self.cond_weight = nn.Parameter(torch.rand(n_outputs, cond_label_size)\
                                            / math.sqrt(cond_label_size))

    def forward(self, x, y=None):
        out = F.linear(x, self.weight * self.mask, self.bias)
        if y is not None:
            out = out + F.linear(y, self.cond_weight)
        return out

    def extra_repr(self):
        return 'in_features={}, out_features={}, bias={}'.format(
            self.in_features, self.out_features, self.bias is not None
        ) + (self.cond_label_size is not None) * ', cond_features={}'.format(self.cond_label_size)

class BatchNorm(nn.Module):
    """ RealNVP BatchNorm layer """
    def __init__(self, input_size, momentum=0.9, eps=1e-5):
        super().__init__()
        self.momentum = momentum
        self.eps = eps

        self.log_gamma = nn.Parameter(torch.zeros(input_size))
        self.beta = nn.Parameter(torch.zeros(input_size))

        self.register_buffer('running_mean', torch.zeros(input_size))
        self.register_buffer('running_var', torch.ones(input_size))

    def forward(self, x, cond_y=None):
        """ Forward pass """
        if self.training:
            self.batch_mean = x.mean(0)
            # note MAF paper uses biased variance estimate; ie x.var(0, unbiased=False)
            self.batch_var = x.var(0)

            # update running mean
            self.running_mean.mul_(self.momentum).add_(self.batch_mean.data * (1 - self.momentum))
            self.running_var.mul_(self.momentum).add_(self.batch_var.data * (1 - self.momentum))

            mean = self.batch_mean
            var = self.batch_var
        else:
            mean = self.running_mean
            var = self.running_var

        # compute normalized input (cf original batch norm paper algo 1)
        x_hat = (x - mean) / torch.sqrt(var + self.eps)
        y = self.log_gamma.exp() * x_hat + self.beta

        # compute log_abs_det_jacobian (cf RealNVP paper)
        log_abs_det_jacobian = self.log_gamma - 0.5 * torch.log(var + self.eps)
        #print_str = 'in sum log var {:6.3f} ; out sum log var {:6.3f}; sum log det {:8.3f}; '+\
        #      'mean log_gamma {:5.3f}; mean beta {:5.3f}'
        #print(print_str.format((var + self.eps).log().sum().data.numpy(),
        #                       y.var(0).log().sum().data.numpy(),
        #                       log_abs_det_jacobian.mean(0).item(), self.log_gamma.mean(),
        #                       self.beta.mean()))
        return y, log_abs_det_jacobian.expand_as(x)

    def inverse(self, y, cond_y=None):
        """ Inverse pass """
        if self.training:
            mean = self.batch_mean
            var = self.batch_var
        else:
            mean = self.running_mean
            var = self.running_var

        x_hat = (y - self.beta) * torch.exp(-self.log_gamma)
        x = x_hat * torch.sqrt(var + self.eps) + mean

        log_abs_det_jacobian = 0.5 * torch.log(var + self.eps) - self.log_gamma

        return x, log_abs_det_jacobian.expand_as(x)


class FlowSequential(nn.Sequential):
    """ Container for layers of a normalizing flow """
    def forward(self, x, y):
        sum_log_abs_det_jacobians = 0
        for module in self:
            x, log_abs_det_jacobian = module(x, y)
            sum_log_abs_det_jacobians = sum_log_abs_det_jacobians + log_abs_det_jacobian
        return x, sum_log_abs_det_jacobians

    def inverse(self, u, y):
        sum_log_abs_det_jacobians = 0
        for module in reversed(self):
            u, log_abs_det_jacobian = module.inverse(u, y)
            sum_log_abs_det_jacobians = sum_log_abs_det_jacobians + log_abs_det_jacobian
        return u, sum_log_abs_det_jacobians

class MADE(nn.Module):
    def __init__(self, input_size, hidden_size, n_hidden, cond_label_size=None,
                 activation='relu', input_order='sequential', input_degrees=None):
        """
        Args:
            input_size -- scalar; dim of inputs
            hidden_size -- scalar; dim of hidden layers
            n_hidden -- scalar; number of hidden layers
            activation -- str; activation function to use
            input_order -- str or tensor; variable order for creating the
                                          autoregressive masks (sequential|random)
                                          or the order flipped from the previous
                                          layer in a stack of mades
        """
        super().__init__()
        # base distribution for calculation of log prob under the model
        self.register_buffer('base_dist_mean', torch.zeros(input_size))
        self.register_buffer('base_dist_var', torch.ones(input_size))

        # create masks
        masks, self.input_degrees = create_masks(input_size, hidden_size, n_hidden,
                                                 input_order, input_degrees)
        # setup activation
        if activation == 'relu':
            activation_fn = nn.ReLU()
        elif activation == 'tanh':
            activation_fn = nn.Tanh()
        else:
            raise ValueError('Check activation function.')

        # construct model
        self.net_input = MaskedLinear(input_size, hidden_size, masks[0], cond_label_size)
        self.net = []
        for m in masks[1:-1]:
            self.net += [activation_fn, MaskedLinear(hidden_size, hidden_size, m)]
        self.net += [activation_fn, MaskedLinear(hidden_size,
                                                 2 * input_size, masks[-1].repeat(2, 1))]
        self.net = nn.Sequential(*self.net)
        # init last layer to 0, so first run gives identity
        nn.init.zeros_(self.net[-1].weight)
        nn.init.zeros_(self.net[-1].bias)

    @property
    def base_dist(self):
        return D.Normal(self.base_dist_mean, self.base_dist_var)

    def forward(self, x, y=None):
        # MAF eq 4 -- return mean and log std
        m, loga = self.net(self.net_input(x, y)).chunk(chunks=2, dim=1)
        u = (x - m) * torch.exp(-loga)

        # MAF eq 5
        log_abs_det_jacobian = - loga
        return u, log_abs_det_jacobian

    def inverse(self, u, y=None, sum_log_abs_det_jacobians=None):
        # MAF eq 3
        D = u.shape[1]
        x = torch.zeros_like(u)
        # run through reverse model
        for i in self.input_degrees:
            m, loga = self.net(self.net_input(x, y)).chunk(chunks=2, dim=1)
            x[:, i] = u[:, i] * torch.exp(loga[:, i]) + m[:, i]
        log_abs_det_jacobian = loga
        return x, log_abs_det_jacobian

    def log_prob(self, x, y=None):
        u, log_abs_det_jacobian = self.forward(x, y)
        return torch.sum(self.base_dist.log_prob(u) + log_abs_det_jacobian, dim=1)


class MAF(nn.Module):
    def __init__(self, n_blocks, input_size, hidden_size, n_hidden, layer, cond_label_size=None,
                 activation='relu', input_order='sequential', batch_norm=True):
        super().__init__()

        # base distribution for calculation of log prob under the model
        self.register_buffer('base_dist_mean', torch.zeros(input_size[layer]))
        self.register_buffer('base_dist_var', torch.ones(input_size[layer]))

        # construct model
        modules = []
        # construct random input_degrees:
        #self.input_degrees = None #old
        input_range = torch.arange(input_size[layer])
        input_prob = torch.ones((n_blocks, len(input_range))) / len(input_range)
        self.input_degrees = input_prob.multinomial(num_samples=len(input_range),
                                                    replacement=False)

        for i in range(n_blocks):
            modules += [MADE(input_size[layer], hidden_size, n_hidden, cond_label_size, activation, input_order, self.input_degrees[i])] #[i])]
            #self.input_degrees = modules[-1].input_degrees.flip(0)
            modules += batch_norm * [BatchNorm(input_size[layer])]

        self.net = FlowSequential(*modules)

    @property
    def base_dist(self):
        return D.Normal(self.base_dist_mean, self.base_dist_var)

    def forward(self, x, y=None):
        return self.net(x, y)

    def inverse(self, u, y=None):
        return self.net.inverse(u, y)

    def log_prob(self, x, y=None):
        u, sum_log_abs_det_jacobians = self.forward(x, y)
        return torch.sum(self.base_dist.log_prob(u) + sum_log_abs_det_jacobians, dim=1)

    def log_prob_px(self, x, y=None):
        # logprobs in pixel space, cf eq. 25 of 1705.07057
        raise NotImplementedError
        dataset_lam = 1e-6 # for MNIST
        u, sum_log_abs_det_jacobians = self.forward(x, y)
        log_p_x = torch.sum(self.base_dist.log_prob(u) + sum_log_abs_det_jacobians, dim=1)
        dim_D = x.shape[-1]
        #factor = torch.ones(x.shape[0])*dim_D * torch.log(torch.tensor((1. - 2.*dataset_lam)/256.))
        factor2 = torch.log(torch.sigmoid(x)).sum(dim=-1) + torch.log((1+torch.sigmoid(x))).sum(dim=-1)
        logprobs_px = log_p_x + self.factor - factor2
        return logprobs_px, logprobs_px / (-dim_D * torch.log(torch.tensor(2.)))

def one_hot(values, num_bins):
    """ one-hot encoding of values into num_bins """
    # values are energies in [0, 1], need to be converted to integers in [0, num_bins-1]
    values *= num_bins
    values = values.type(torch.long)
    ret = F.one_hot(values, num_bins)
    return ret.squeeze().double()

def one_blob(values, num_bins):
    """ one-blob encoding of values into num_bins, cf sec. 4.3 of 1808.03856 """
    # torch.tile() not yet in stable release, use numpy instead
    values = values.numpy()[..., np.newaxis]
    y = np.tile(((0.5/num_bins) + np.arange(0., 1., step=1./num_bins)), values.shape)
    res = np.exp(((-num_bins*num_bins)/2.)
                 * (y-values)**2)
    res = np.reshape(res, (-1, values.shape[-1]*num_bins))
    return torch.tensor(res)

@torch.no_grad()
def evaluate(model, dataloader, epoch, args):
    """Evaluate the model """
    model.eval()
    loglike = []
    used_data = []
    used_energy = []
    for data in dataloader:

        x0 = data['layer_0']
        #x1 = data['layer_1']
        #x2 = data['layer_2']
        if args.energy_encoding == 'direct':
            energy = data['energy']
        elif args.energy_encoding == 'one_hot':
            energy = one_hot(data['energy'], args.energy_encoding_bins)
        elif args.energy_encoding == 'one_blob':
            energy = one_blob(data['energy'], args.energy_encoding_bins)

        y = energy.to(args.device)
        x = x0.view(x0.shape[0], -1).to(args.device)
        loglike.append(model.log_prob(x, y))
        used_data.append(x)
        used_energy.append(y)
        #loglike.append(model.log_prob(x))

    logprobs = torch.cat(loglike, dim=0).to(args.device)

    used_data_cat = torch.cat(used_data, dim=0).to(args.device)
    used_energy_cat = torch.cat(used_energy, dim=0).to(args.device)

    logprob_mean = logprobs.mean(0)
    logprob_std = logprobs.var(0).sqrt() / math.sqrt(len(dataloader.dataset))

    output = 'Evaluate ' + (epoch is not None)*'(epoch {}) -- '.format(epoch+1) +\
        'logp(x, at E(x)) = {:.3f} +/- {:.3f}'

    print(output.format(logprob_mean, logprob_std))
    print(output.format(logprob_mean, logprob_std), file=open(args.results_file, 'a'))
    #debugging:
    if logprob_mean != logprob_mean:
        print("NaN triggered")
        which_ones = torch.isnan(logprobs)
        print("logprobs: ", logprobs[which_ones])
        print("data: ", used_data_cat[which_ones])
        print("energy: ", used_energy_cat[which_ones])
    return logprob_mean, logprob_std

@torch.no_grad()
def evaluate_cond(model, dataloader, epoch, args, num_pts):
    """ Evaluate the model, integrating over the conditional using num_pts points"""
    model.eval()
    # assume flat prior
    logprior = torch.tensor(1./num_pts).log().to(args.device)
    loglike = [[] for _ in range(num_pts)]
    energies = torch.arange(0., 1., 1./num_pts) + 0.5/num_pts

    for i in range(num_pts):
        if args.energy_encoding == 'direct':
            energy = torch.reshape(energies[i], (-1, 1))
        elif args.energy_encoding == 'one_hot':
            energy = one_hot(energies[i], args.energy_encoding_bins)
        elif args.energy_encoding == 'one_blob':
            energy = one_blob(energies[i], args.energy_encoding_bins)
        for data in dataloader:
            x0 = data['layer_0']
            local_energy = energy.repeat((x0.shape[0], 1)).to(args.device)
            x = x0.view(x0.shape[0], -1).to(args.device)
            loglike[i].append(model.log_prob(x, local_energy))

        loglike[i] = torch.cat(loglike[i], dim=0)
        print("Evaluation done to {}%".format(100.*(i+1)/num_pts), end='\r')
    loglike = torch.stack(loglike, dim=1)
    logprobs = logprior + loglike.logsumexp(dim=1)

    logprob_mean = logprobs.mean(0)
    logprob_std = logprobs.var(0).sqrt() / math.sqrt(len(dataloader.dataset))

    output = 'Cond. Evaluate ' + (epoch is not None)*'(epoch {}) -- '.format(epoch+1) +\
        'logp(x) = {:.3f} +/- {:.3f}'

    print(output.format(logprob_mean, logprob_std))
    print(output.format(logprob_mean, logprob_std), file=open(args.results_file, 'a'))
    return logprob_mean, logprob_std

@torch.no_grad()
def generate(model, args, energies=None, n_col=10, step=None, include_average=False):
    """ generate samples from the trained model"""
    model.eval()

    if energies is None:
        energies = torch.tensor([10., 20., 30., 40., 50., 60., 70., 80., 90.]) / 100.
        #energies = torch.tensor([10., 50., 90.]) / 100.
    if args.energy_encoding == 'direct':
        energies = torch.reshape(energies, (-1, 1)).to(args.device)
    elif args.energy_encoding == 'one_hot':
        energies = one_hot(energies, args.energy_encoding_bins).to(args.device)
    elif args.energy_encoding == 'one_blob':
        energies = one_blob(energies, args.energy_encoding_bins).to(args.device)

    u = model.base_dist.sample((n_col*len(energies),)).squeeze()
    energies = energies.repeat((n_col, 1))
    samples, _ = model.inverse(u, energies)
    #samples, _ = model.inverse(u)

    # sort by log_prob; take argsort idxs; flip high to low
    #log_probs = model.log_prob(samples).sort(0)[1].flip(0)
    #samples = samples[log_probs]

    # reshape and transform back to energy
    samples = samples.view(samples.shape[0], *args.input_dims['0'])
    samples = ((torch.sigmoid(samples) - 1e-6) / (1. - 2e-6))*1e5
    filename = 'generated_samples' + (step is not None)*'_epoch_{}'.format(step) + '.png'

    # rewrite save_image to calodata!
    plot_calo.plot_calo_batch(samples, os.path.join(args.output_dir, filename), 0, ncol=n_col)

    if include_average:
        num_pts = 10000
        energies = 0.99*torch.rand((num_pts,)) + 0.01
        if args.energy_encoding == 'direct':
            energies = torch.reshape(energies, (-1, 1)).to(args.device)
        elif args.energy_encoding == 'one_hot':
            energies = one_hot(energies, args.energy_encoding_bins).to(args.device)
        elif args.energy_encoding == 'one_blob':
            energies = one_blob(energies, args.energy_encoding_bins).to(args.device)

        u = model.base_dist.sample((num_pts,)).squeeze()
        samples, _ = model.inverse(u, energies)
        samples = samples.view(samples.shape[0], *args.input_dims['0'])
        samples, num_bad = remove_nans(samples)
        if not num_bad==num_pts:
            print("Removed {} samples of {} from average/distribution plots".format(num_bad, num_pts))
            samples = ((torch.sigmoid(samples) - 1e-6) / (1. - 2e-6))*1e5
            filename = 'generated_samples_avg' + (step is not None)*'_epoch_{}'.format(step) + '.png'
            plot_calo.plot_average_voxel(samples,
                                         os.path.join(args.output_dir, filename),
                                         0, vmin=None)
            filename = 'generated_samples_E_layer_0' + (step is not None)*'_epoch_{}'.format(step) + '.png'
            plot_calo.plot_layer_energy(samples,
                                        os.path.join(args.output_dir, filename),
                                        0)
        else:
            print("Skipping plotting due to bad sample")


def train(model, dataloader, optimizer, epoch, args):

    for i, data in enumerate(dataloader):
        model.train()

        x0 = data['layer_0']
        x1 = data['layer_1']
        x2 = data['layer_2']
        if args.energy_encoding == 'direct':
            energy = data['energy']
        elif args.energy_encoding == 'one_hot':
            energy = one_hot(data['energy'], args.energy_encoding_bins)
        elif args.energy_encoding == 'one_blob':
            energy = one_blob(data['energy'], args.energy_encoding_bins)

        y = energy.to(args.device)
        x = x0.view(x0.shape[0], -1).to(args.device)

        loss = - model.log_prob(x, y).mean(0)
        #loss = -model.log_prob(x).mean(0)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if i % args.log_interval == 0:
            print('epoch {:3d} / {}, step {:4d} / {}; loss {:.4f}'.format(
                epoch+1, args.start_epoch + args.n_epochs, i, len(dataloader), loss.item()))
            print('epoch {:3d} / {}, step {:4d} / {}; loss {:.4f}'.format(
                epoch+1, args.start_epoch + args.n_epochs, i, len(dataloader), loss.item()),
                  file=open(args.results_file, 'a'))

def train_and_evaluate(model, train_loader, test_loader, optimizer, args):
    """As the name says"""
    best_eval_logprob = float('-inf')

    #lr_schedule = torch.optim.lr_scheduler.ExponentialLR(optimizer, 0.85, verbose=True)
    for i in range(args.start_epoch, args.start_epoch + args.n_epochs):
        train(model, train_loader, optimizer, i, args)
        #eval_logprob, _ = evaluate_cond(model, test_loader, i, args, 20)
        with torch.no_grad():
            eval_logprob, _ = evaluate(model, test_loader, i, args)

        # save training checkpoint
        torch.save({'epoch': i,
                    'model_state': model.state_dict(),
                    'optimizer_state': optimizer.state_dict()},
                   os.path.join(args.output_dir, 'model_checkpoint.pt'))
        # save model only
        torch.save(model.state_dict(), os.path.join(args.output_dir, 'model_state.pt'))

        # save best state
        if eval_logprob > best_eval_logprob:
            best_eval_logprob = eval_logprob
            torch.save({'epoch': i,
                        'model_state': model.state_dict(),
                        'optimizer_state': optimizer.state_dict()},
                       os.path.join(args.output_dir, 'best_model_checkpoint.pt'))

        # plot sample
        with torch.no_grad():
            generate(model, args, step=i+1, include_average=True)
        #lr_schedule.step()
    eval_logprob, _ = evaluate_cond(model, test_loader, i, args, 25)

def remove_nans(tensor):
    """removes elements in the given batch that contain nans
       returns the new tensor and the number of removed elements"""
    tensor_flat = tensor.flatten(start_dim=1)
    good_entries = torch.all(tensor_flat==tensor_flat, axis=1)
    res_flat = tensor_flat[good_entries]
    tensor_shape = list(tensor.size())
    tensor_shape[0] = -1
    res = res_flat.reshape(tensor_shape)
    return res, len(tensor) - len(res)

if __name__ == '__main__':
    args = parser.parse_args()

    # check if output_dir exists
    if not os.path.isdir(args.output_dir):
        os.makedirs(args.output_dir)

    # setup device
    args.device = torch.device('cuda:0' if torch.cuda.is_available() and not args.no_cuda\
                               else 'cpu')
    print("Using {}".format(args.device))

    assert args.particle_type in ['gamma', 'eplus', 'piplus'], \
        'Particle type must be either gamma, eplus, or piplus!'

    if args.plot_true:
        # set batch_size to 1, so it can be easier organized by energy
        args.batch_size = 64

    if args.use_full:
        train_dataloader = get_dataloader(args.particle_type, args.data_dir, full=True,
                                          apply_logit=True, device=args.device,
                                          batch_size=args.batch_size)

    else:
        train_dataloader, test_dataloader = get_dataloader(args.particle_type, args.data_dir,
                                                           full=False, apply_logit=True,
                                                           device=args.device,
                                                           batch_size=args.batch_size)
    if args.energy_encoding == 'direct':
        cond_label_size = 1
    elif args.energy_encoding in ['one_hot', 'one_blob']:
        cond_label_size = int(args.energy_encoding_bins)
    else:
        raise ValueError('energy_encoding not in ["direct", "one_hot", "one_blob"]')

    args.input_size = train_dataloader.dataset.input_size
    args.input_dims = train_dataloader.dataset.input_dims


    model = MAF(args.n_blocks, args.input_size, args.hidden_size, args.n_hidden,
                cond_label_size=cond_label_size, activation=args.activation_fn,
                input_order=args.input_order, batch_norm=not args.no_batch_norm,
                layer='0')

    model = model.to(args.device)
    optimizer = torch.optim.AdamW(model.parameters(), lr=args.lr, weight_decay=1e-5)

    if args.restore_file:
        # load model and optimizer states
        state = torch.load(args.restore_file, map_location=args.device)
        model.load_state_dict(state['model_state'])
        optimizer.load_state_dict(state['optimizer_state'])
        args.start_epoch = state['epoch'] + 1
        # set up paths
        args.output_dir = os.path.dirname(args.restore_file)
    args.results_file = os.path.join(args.output_dir, args.results_file)

    print('Loaded settings and model:')
    #print(pprint.pformat(args.__dict__))
    print(model)
    #print(pprint.pformat(args.__dict__), file=open(args.results_file, 'a'))
    #print(model, file=open(args.results_file, 'a'))

    num_parameters = sum(p.numel() for p in model.parameters() if p.requires_grad)
    output_str = "{} parameters in each of the {} blocks, {} total"
    print(output_str.format(int(num_parameters/args.n_blocks), args.n_blocks,
                            num_parameters))
                

    if args.play:
        print(len(train_dataloader))
        print(len(train_dataloader.dataset))
        if not args.use_full:
            print(len(test_dataloader))
            print(len(test_dataloader.dataset))

    if args.plot_true:
        for x in train_dataloader:
            sample = x['layer_0']

            sorted_order = torch.argsort(x['energy'].squeeze())
            sample = ((torch.sigmoid(sample) - 1e-6) / (1. - 2e-6))*1e5
            filename = args.particle_type + '_training_sample.png'
            plot_calo.plot_calo_batch(sample[sorted_order],
                                      os.path.join(args.output_dir, filename),
                                      0, ncol=8)
            print("Energies: ", x['energy'][sorted_order]*100.)
            print("plotting target sample done")
            break

    if args.plot_true_avg:
        average_dataloader = get_dataloader(args.particle_type, args.data_dir,
                                            full=True,
                                            apply_logit=True,
                                            device=args.device,
                                            batch_size=100000)

        for x in average_dataloader:
            data = x['layer_0']
            break
        data = ((torch.sigmoid(data) - 1e-6) / (1. - 2e-6))*1e5
        filename = args.particle_type + '_target_avg.png'
        plot_calo.plot_average_voxel(data,
                                     os.path.join(args.output_dir, filename),
                                     0, vmin=None)
        print("plotting target average done")

        # testing energy_per_layer plot:
        #filename = args.particle_type + '_target_E_layer_0.png'
        #plot_calo.plot_layer_energy(data,
        #                            os.path.join(args.output_dir, filename),
        #                            0)

    if args.train:
        train_and_evaluate(model, train_dataloader, test_dataloader, optimizer, args)

    if args.generate:
        # add energy parser, n_sample parser
        generate(model, args, energies=None, n_col=10, include_average=True)
