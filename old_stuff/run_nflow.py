"""
CaloFlow: Normalizing Flows for Calorimeter Shower generation
code based on https://github.com/bayesiains/nflows and https://arxiv.org/pdf/1906.04032.pdf

"""

import argparse
import os

import torch
import torch.nn.functional as F
import numpy as np
from nflows import transforms, distributions, flows
from nflows.nn import nets as nflownets

import plot_calo
from data import get_dataloader

torch.set_default_dtype(torch.float64)

parser = argparse.ArgumentParser()

# usage modes
parser.add_argument('--train', action='store_true', help='train a flow')
parser.add_argument('--generate', action='store_true', help='generate from a trained flow')

parser.add_argument('--plot_true', action='store_true',
                    help='plot a random batch of train data ordered by energy')
parser.add_argument('--plot_true_avg', action='store_true',
                    help='plot average of all training data')
parser.add_argument('--no_cuda', action='store_true', help='Do not use cuda.')
parser.add_argument('--restore_file', type=str, help='Path to model to restore.')
parser.add_argument('--output_dir', default='./results', help='Where to store the output')
parser.add_argument('--data_dir', default='/media/claudius/8491-9E93/ML_sources/CaloGAN')
parser.add_argument('--results_file', default='results.txt',
                    help='Filename where to store settings and test results.')

# CALO specific
parser.add_argument('--particle_type', '-p',
                    help='Which particle to shower, gamma, eplus, or piplus')
parser.add_argument('--num_layer', default=0, type=int,
                    help='How many Calolayers are trained')
parser.add_argument('--energy_encoding', default='direct',
                    help='How the energy conditional is given to the NN: "direct", '+\
                    '"one_hot" or "one_blob"')
parser.add_argument('--energy_encoding_bins', default=100,
                    help='Number of bins in one_hot or one_blob energy encoding')
parser.add_argument('--use_full', '-u', action='store_true',
                    help='Train on full dataset, no test set')

# MAF parameters
parser.add_argument('--n_blocks', type=int, default=10,
                    help='Total number of blocks to stack in a model (MADE in MAF).')
parser.add_argument('--n_blocks_checker', type=int, default=1,
                    help='Number of checkerboard blocks (each blocks contains 2 maskings). '+\
                    'Must be smaller than n_blocks / 2')
parser.add_argument('--hidden_size', type=int, default=864,
                    help='Hidden layer size for each MADE block in an MAF.')
parser.add_argument('--n_hidden', type=int, default=1,
                    help='Number of hidden layers in each MADE.')
parser.add_argument('--activation_fn', type=str, default='relu',
                    help='What activation function of torch.nn.functional to use in the MADEs.')
#parser.add_argument('--loss_energy', type=float, default=0.,
#                    help='factor of energy reconstruction in loss')
parser.add_argument('--input_order', type=str, default='sequential',
                    help='What input order to use (sequential | random).')
parser.add_argument('--batch_norm', action='store_true', default=False,
                    help='Use batch normalization')
parser.add_argument('--n_bins', type=int, default=8,
                    help='Number of bins if piecewise transforms are used')
parser.add_argument('--use_residual', action='store_true', default=False,
                    help='Use residual layers in the NNs')
parser.add_argument('--dropout_probability', '-d', type=float, default=0., help='dropout probability')
parser.add_argument('--tail_bound', type=float, default=14., help='Domain of the RQS')
parser.add_argument('--cond_base', action='store_true',
                    help='Use Gaussians conditioned on energy as base distribution.')
parser.add_argument('--no_layer_cond', action='store_true',
                    help='do not condition layers 1 and 2 on the ouput of layers 0 and 1')

# training params
parser.add_argument('--batch_size', type=int, default=200)
parser.add_argument('--n_epochs', type=int, default=50)
parser.add_argument('--lr', type=float, default=4e-5, help='Learning rate.')
parser.add_argument('--log_interval', type=int, default=175,
                    help='How often to show loss statistics and save samples.')

# used in transformation between energy and logit space:
# (should match the ALPHA in data.py)
ALPHA = 1e-6

def logit(x):
    return torch.log(x / (1.0 - x))

def logit_trafo(x):
    local_x = ALPHA + (1. - 2.*ALPHA) * x
    return logit(local_x)

def inverse_logit(x):
    return ((torch.sigmoid(x) - ALPHA) / (1. - 2.*ALPHA))

def one_hot(values, num_bins):
    """ one-hot encoding of values into num_bins """
    # values are energies in [0, 1], need to be converted to integers in [0, num_bins-1]
    values *= num_bins
    values = values.type(torch.long)
    ret = F.one_hot(values, num_bins)
    return ret.squeeze().double()

def one_blob(values, num_bins):
    """ one-blob encoding of values into num_bins, cf sec. 4.3 of 1808.03856 """
    # torch.tile() not yet in stable release, use numpy instead
    values = values.numpy()[..., np.newaxis]
    y = np.tile(((0.5/num_bins) + np.arange(0., 1., step=1./num_bins)), values.shape)
    res = np.exp(((-num_bins*num_bins)/2.)
                 * (y-values)**2)
    res = np.reshape(res, (-1, values.shape[-1]*num_bins))
    return torch.tensor(res)

def remove_nans(tensor):
    """removes elements in the given batch that contain nans
       returns the new tensor and the number of removed elements"""
    tensor_flat = tensor.flatten(start_dim=1)
    good_entries = torch.all(tensor_flat==tensor_flat, axis=1)
    res_flat = tensor_flat[good_entries]
    tensor_shape = list(tensor.size())
    tensor_shape[0] = -1
    res = res_flat.reshape(tensor_shape)
    return res, len(tensor) - len(res)

@torch.no_grad()
def generate(model, args, energies=None, n_col=10, step=None, include_average=False):
    """ generate samples from the trained model"""
    model.eval()

    if energies is None:
        #energies = torch.tensor([10., 20., 30., 40., 50., 60., 70., 80., 90.]) / 100.
        energies = torch.arange(0.01, 1.01, 0.01)
        energies[-1] -= 1e-6
    if args.energy_encoding == 'direct':
        energies = torch.reshape(energies, (-1, 1)).to(args.device)
    elif args.energy_encoding == 'one_hot':
        energies = one_hot(energies, args.energy_encoding_bins).to(args.device)
    elif args.energy_encoding == 'one_blob':
        energies = one_blob(energies, args.energy_encoding_bins).to(args.device)

    samples = model.sample(1, energies)
    sample_list = [*torch.split(samples, args.dim_split, dim=-1)]

    for plot_layer_id, sample in enumerate(sample_list):
        # reshape and transform back to energy
        sample = sample.view(-1, *args.input_dims[str(plot_layer_id)])

        sample = ((torch.sigmoid(sample) - ALPHA) / (1. - 2.*ALPHA))*1e5
        filename = 'generated_samples_layer_'+str(plot_layer_id) +\
            (step is not None)*'_epoch_{}'.format(step) + '.png'

        plot_calo.plot_calo_batch(sample, os.path.join(args.output_dir, filename),
                                  plot_layer_id, ncol=n_col)
    if include_average:
        num_pts = 10000
        energies = 0.99*torch.rand((num_pts,)) + 0.01
        if args.energy_encoding == 'direct':
            energies = torch.reshape(energies, (-1, 1)).to(args.device)
        elif args.energy_encoding == 'one_hot':
            energies = one_hot(energies, args.energy_encoding_bins).to(args.device)
        elif args.energy_encoding == 'one_blob':
            energies = one_blob(energies, args.energy_encoding_bins).to(args.device)

        samples = model.sample(1, energies)
        sample_list = [*torch.split(samples, args.dim_split, dim=-1)]

        for plot_layer_id, sample in enumerate(sample_list):
            sample = sample.view(num_pts, *args.input_dims[str(plot_layer_id)])
            sample, num_bad = remove_nans(sample)

            if not num_bad == num_pts:
                print("Removed {} samples of {} from average/distribution plots".format(num_bad,
                                                                                        num_pts))
                print(sample.to('cpu').detach().numpy().min(),
                      sample.to('cpu').detach().numpy().max())
                sample = ((torch.sigmoid(sample) - ALPHA) / (1. - 2.*ALPHA))*1e5
                print(sample.to('cpu').detach().numpy().min(),
                      sample.to('cpu').detach().numpy().max())
                filename = 'generated_samples_avg_layer_'+str(plot_layer_id) +\
                    (step is not None)*'_epoch_{}'.format(step) + '.png'
                plot_calo.plot_average_voxel(sample,
                                             os.path.join(args.output_dir, filename),
                                             plot_layer_id, vmin=None)

                filename = 'generated_samples_E_layer_'+str(plot_layer_id) +\
                    (step is not None)*'_epoch_{}'.format(step) + '.png'
                plot_calo.plot_layer_energy(sample,
                                            os.path.join(args.output_dir, filename),
                                            plot_layer_id, plot_ref=args.particle_type,
                                            epoch_nr=step)
            else:
                print("Skipping plotting due to bad sample")

        if args.num_layer == 2:
            filename = 'generated_samples_E_total' +\
                       (step is not None)*'_epoch_{}'.format(step) + '.png'

            samples = ((torch.sigmoid(samples) - ALPHA) / (1. - 2.*ALPHA))*1e5

            plot_calo.plot_total_energy(samples,
                                        os.path.join(args.output_dir, filename),
                                        plot_ref=args.particle_type,
                                        epoch_nr=step)
            for layer in [0, 1, 2]:
                for use_log in [False, True]:
                    filename = 'energy_fraction_layer_'+str(layer)+\
                               (use_log)*'_log'+\
                               (step is not None)*'_epoch_{}'.format(step) + '.png'
                    plot_calo.plot_energy_fraction(samples,
                                                   os.path.join(args.output_dir, filename),
                                                   layer, plot_ref=args.particle_type,
                                                   epoch_nr=step, use_log=use_log)

@torch.no_grad()
def evaluate_cond(model, dataloader, epoch, args, num_pts):
    """ Evaluate the model, integrating over the conditional using num_pts points"""
    model.eval()
    # assume flat prior
    logprior = torch.tensor(1./num_pts).log().to(args.device)
    loglike = [[] for _ in range(num_pts)]
    energies = torch.arange(0., 1., 1./num_pts) + 0.5/num_pts

    for i in range(num_pts):
        if args.energy_encoding == 'direct':
            energy = torch.reshape(energies[i], (-1, 1))
        elif args.energy_encoding == 'one_hot':
            energy = one_hot(energies[i], args.energy_encoding_bins)
        elif args.energy_encoding == 'one_blob':
            energy = one_blob(energies[i], args.energy_encoding_bins)
        for data in dataloader:
            x0 = data['layer_0']
            x1 = data['layer_1']
            x2 = data['layer_2']
            local_energy = energy.repeat((x0.shape[0], 1)).to(args.device)
            if args.num_layer == 0:
                del x1, x2
                x = x0.view(x0.shape[0], -1).to(args.device)
            elif args.num_layer == 1:
                del x2
                layer0 = x0.view(x0.shape[0], -1)
                layer1 = x1.view(x1.shape[0], -1)
                x = torch.cat((layer0, layer1), 1).to(args.device)
            elif args.num_layer == 2:
                layer0 = x0.view(x0.shape[0], -1)
                layer1 = x1.view(x1.shape[0], -1)
                layer2 = x2.view(x2.shape[0], -1)
                x = torch.cat((layer0, layer1, layer2), 1).to(args.device)

            loglike[i].append(model.log_prob(x, local_energy))

        loglike[i] = torch.cat(loglike[i], dim=0)
        print("Evaluation done to {}%".format(100.*(i+1)/num_pts), end='\r')
    loglike = torch.stack(loglike, dim=1)
    logprobs = logprior + loglike.logsumexp(dim=1)

    logprob_mean = logprobs.mean(0)
    logprob_std = logprobs.var(0).sqrt() / np.sqrt(len(dataloader.dataset))

    output = 'Cond. Evaluate ' + (epoch is not None)*'(epoch {}) -- '.format(epoch+1) +\
        'logp(x) = {:.3f} +/- {:.3f}'

    print(output.format(logprob_mean, logprob_std))
    print(output.format(logprob_mean, logprob_std), file=open(args.results_file, 'a'))
    return logprob_mean, logprob_std

@torch.no_grad()
def evaluate(model, dataloader, epoch, args):
    """Evaluate the model, i.e find the mean log_prob of the test set
       Energy is taken to be the energy of the image, so no
       marginalization is performed.
    """
    model.eval()
    loglike = []
    for data in dataloader:

        x0 = data['layer_0']
        x1 = data['layer_1']
        x2 = data['layer_2']
        if args.energy_encoding == 'direct':
            energy = data['energy']
        elif args.energy_encoding == 'one_hot':
            energy = one_hot(data['energy'], args.energy_encoding_bins)
        elif args.energy_encoding == 'one_blob':
            energy = one_blob(data['energy'], args.energy_encoding_bins)

        y = energy.to(args.device)
        if args.num_layer == 0:
            del x1, x2
            x = x0.view(x0.shape[0], -1).to(args.device)
        elif args.num_layer == 1:
            del x2
            layer0 = x0.view(x0.shape[0], -1)
            layer1 = x1.view(x1.shape[0], -1)
            x = torch.cat((layer0, layer1), 1).to(args.device)
        elif args.num_layer == 2:
            layer0 = x0.view(x0.shape[0], -1)
            layer1 = x1.view(x1.shape[0], -1)
            layer2 = x2.view(x2.shape[0], -1)
            x = torch.cat((layer0, layer1, layer2), 1).to(args.device)

        loglike.append(model.log_prob(x, y))

    logprobs = torch.cat(loglike, dim=0).to(args.device)

    logprob_mean = logprobs.mean(0)
    logprob_std = logprobs.var(0).sqrt() / np.sqrt(len(dataloader.dataset))

    output = 'Evaluate ' + (epoch is not None)*'(epoch {}) -- '.format(epoch+1) +\
        'logp(x, at E(x)) = {:.3f} +/- {:.3f}'

    print(output.format(logprob_mean, logprob_std))
    print(output.format(logprob_mean, logprob_std), file=open(args.results_file, 'a'))
    return logprob_mean, logprob_std

def train(model, dataloader, optimizer, epoch, args):

    for i, data in enumerate(dataloader):
        model.train()

        x0 = data['layer_0']
        x1 = data['layer_1']
        x2 = data['layer_2']
        if args.energy_encoding == 'direct':
            energy = data['energy']
        elif args.energy_encoding == 'one_hot':
            energy = one_hot(data['energy'], args.energy_encoding_bins)
        elif args.energy_encoding == 'one_blob':
            energy = one_blob(data['energy'], args.energy_encoding_bins)

        y = energy.to(args.device)
        if args.num_layer == 0:
            del x1, x2
            x = x0.view(x0.shape[0], -1).to(args.device)
        elif args.num_layer == 1:
            del x2
            layer0 = x0.view(x0.shape[0], -1)
            layer1 = x1.view(x1.shape[0], -1)
            x = torch.cat((layer0, layer1), 1).to(args.device)
        elif args.num_layer == 2:
            layer0 = x0.view(x0.shape[0], -1)
            layer1 = x1.view(x1.shape[0], -1)
            layer2 = x2.view(x2.shape[0], -1)
            x = torch.cat((layer0, layer1, layer2), 1).to(args.device)

        loss = - model.log_prob(x, y).mean(0)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        args.train_loss.append(loss.tolist())

        if i % args.log_interval == 0:
            print('epoch {:3d} / {}, step {:4d} / {}; loss {:.4f}'.format(
                epoch+1, args.n_epochs, i, len(dataloader), loss.item()))
            print('epoch {:3d} / {}, step {:4d} / {}; loss {:.4f}'.format(
                epoch+1, args.n_epochs, i, len(dataloader), loss.item()),
                  file=open(args.results_file, 'a'))

def train_and_evaluate(model, train_loader, test_loader, optimizer, args):
    """As the name says, train the flow and evaluate after each epoch
       TO-DO: implement learning rate schedule
    """
    best_eval_logprob = float('-inf')

    #lr_schedule = torch.optim.lr_scheduler.ExponentialLR(optimizer, 0.85, verbose=True)
    for i in range(args.n_epochs):
        train(model, train_loader, optimizer, i, args)
        #eval_logprob, _ = evaluate_cond(model, test_loader, i, args, 20)
        with torch.no_grad():
            eval_logprob, _ = evaluate(model, test_loader, i, args)
            args.test_loss.append(-eval_logprob.to('cpu').numpy())

        # save training checkpoint
        torch.save({'epoch': i,
                    'model_state': model.state_dict(),
                    'optimizer_state': optimizer.state_dict()},
                   os.path.join(args.output_dir, 'model_checkpoint.pt'))
        # save model only
        torch.save(model.state_dict(), os.path.join(args.output_dir, 'model_state.pt'))

        # save best state
        if eval_logprob > best_eval_logprob:
            best_eval_logprob = eval_logprob
            torch.save({'epoch': i,
                        'model_state': model.state_dict(),
                        'optimizer_state': optimizer.state_dict()},
                       os.path.join(args.output_dir, 'best_model_checkpoint.pt'))

        # plot sample
        with torch.no_grad():
            generate(model, args, step=i+1, include_average=True)
        #lr_schedule.step()
        plot_calo.plot_loss(args.train_loss, args.test_loss,
                            os.path.join(args.output_dir, 'loss.png'))
    with torch.no_grad():
        eval_logprob, _ = evaluate_cond(model, test_loader, i, args, 25)

class BaseContext(torch.nn.Module):
    """ Small NN to map energy input to mean and width of base_gaussians"""
    def __init__(self, context_size, dimensionality):
        """ context_size: length of context vector
            dimensionality: number of dimensions of base dist.
        """
        super(BaseContext, self).__init__()
        self.layer1 = torch.nn.Linear(context_size, 32)
        self.layer2 = torch.nn.Linear(32, 32)
        self.output = torch.nn.Linear(32, 2*dimensionality)

    def forward(self, x):
        x = F.relu(self.layer1(x))
        x = F.relu(self.layer2(x))
        out = F.relu(self.output(x))
        return out

class MultiStepTransformNoCond(torch.nn.Module):
    """ Transform that is able to handle calo problem:
        have one flow for each layer, but condition only on energy
    """
    def __init__(self, list_of_dims, list_of_transforms):
        """Constructor.
        Args:
            list_of_dims: a list of the dimensions of the Steps
            list_of_transforms: A list of `Transform` objects.
        """
        super().__init__()
        self._dims = list_of_dims
        self._transforms = torch.nn.ModuleList(list_of_transforms)
        assert len(list_of_dims) == len(list_of_transforms), (
            "number of given dimensions does not match number of transformations")

    def forward(self, inputs, context=None):
        input_list = [*torch.split(inputs, self._dims, dim=1)]
        output_list = []
        log_absdet = torch.zeros(inputs.shape[0]).to(args.device)
        for step in range(len(self._dims)):
            output_list.append(self._transforms[step](input_list[step], context=context)[0])
            log_absdet += self._transforms[step](input_list[step], context=context)[1]
        if len(self._dims) == 1:
            ret = output_list[0]
        else:
            ret = torch.cat(tuple(output_list), dim=1)
        return ret, log_absdet

    def inverse(self, inputs, context=None):
        input_list = [*torch.split(inputs, self._dims, dim=1)]
        output_list = []
        log_absdet = torch.zeros(inputs.shape[0]).to(args.device)
        for step in range(len(self._dims)):
            output_list.append(self._transforms[step].inverse(input_list[step], context=context)[0])
            log_absdet += self._transforms[step].inverse(input_list[step], context=context)[1]
        if len(self._dims) == 1:
            ret = output_list[0]
        else:
            ret = torch.cat(tuple(output_list), dim=1)
        return ret, log_absdet

class MultiStepTransform(torch.nn.Module):
    """ Transform that is able to handle calo problem:
        transform part of data first, then transform next part conditioned on it.
    """
    def __init__(self, list_of_dims, list_of_transforms):
        """Constructor.
        Args:
            list_of_dims: a list of the dimensions of the Steps
            list_of_transforms: A list of `Transform` objects.
        """
        super().__init__()
        self._dims = list_of_dims
        self._transforms = torch.nn.ModuleList(list_of_transforms)
        assert len(list_of_dims) == len(list_of_transforms), (
            "number of given dimensions does not match number of transformations")

    def forward(self, inputs, context=None):
        # context=None case?
        input_list = [*torch.split(inputs, self._dims, dim=1)]
        output_list = []
        log_absdet = torch.zeros(inputs.shape[0]).to(args.device)
        for step in range(len(self._dims)):
            use_context = context
            if step > 0:
                internal_context = torch.log10(inverse_logit(
                    torch.cat(tuple(output_list), dim=1))+2e-7)+3.75
                use_context = torch.cat((internal_context, context), 1)
            output_list.append(self._transforms[step](input_list[step], context=use_context)[0])
            log_absdet += self._transforms[step](input_list[step], context=use_context)[1]

        if len(self._dims) == 1:
            ret = output_list[0]
        else:
            ret = torch.cat(tuple(output_list), dim=1)
        return ret, log_absdet

    def inverse(self, inputs, context=None):
        # context=None case?
        input_list = [*torch.split(inputs, self._dims, dim=1)]
        output_list = []
        log_absdet = torch.zeros(inputs.shape[0]).to(args.device)
        for step in range(len(self._dims)):
            use_context = context
            if step > 0:
                internal_context = torch.log10(inverse_logit(
                    torch.cat(tuple(output_list), dim=1))+2e-7)+3.75
                use_context = torch.cat((internal_context, context), 1)
            output_list.append(self._transforms[step].inverse(input_list[step],
                                                              context=use_context)[0])
            log_absdet += self._transforms[step].inverse(input_list[step], context=use_context)[1]

        if len(self._dims) == 1:
            ret = output_list[0]
        else:
            ret = torch.cat(tuple(output_list), dim=1)
        return ret, log_absdet

if __name__ == '__main__':
    args = parser.parse_args()

    assert 2* args.n_blocks_checker <= args.n_blocks, (
        'Cannot have {} checker blocks when only {} total blocks'.format(
            2* args.n_blocks_checker, args.n_blocks))
    assert args.num_layer in [0, 1, 2], (
        "Calorimeter only has layer 0, 1, or 2")

    # check if output_dir exists
    if not os.path.isdir(args.output_dir):
        os.makedirs(args.output_dir)

    # setup device
    args.device = torch.device('cuda:0' if torch.cuda.is_available() and not args.no_cuda\
                               else 'cpu')
    print("Using {}".format(args.device))

    assert args.particle_type in ['gamma', 'eplus', 'piplus'], \
        'Particle type must be either gamma, eplus, or piplus!'

    if args.plot_true:
        print("setting batch size to 100 to plot pretty target plot")
        args.batch_size = 100

    if args.use_full:
        train_dataloader = get_dataloader(args.particle_type, args.data_dir, full=True,
                                          apply_logit=True, device=args.device,
                                          batch_size=args.batch_size)

    else:
        train_dataloader, test_dataloader = get_dataloader(args.particle_type, args.data_dir,
                                                           full=False, apply_logit=True,
                                                           device=args.device,
                                                           batch_size=args.batch_size)
    if args.energy_encoding == 'direct':
        cond_label_size = 1
    elif args.energy_encoding in ['one_hot', 'one_blob']:
        cond_label_size = int(args.energy_encoding_bins)
    else:
        raise ValueError('energy_encoding not in ["direct", "one_hot", "one_blob"]')

    args.input_size = train_dataloader.dataset.input_size
    args.input_dims = train_dataloader.dataset.input_dims
    args.train_loss = []
    args.test_loss = []

    flow_transforms = []
    list_of_dims = []

    flow_params_RQS = {'hidden_features':args.hidden_size,
                       'num_blocks':args.n_hidden,
                       'use_residual_blocks':args.use_residual,
                       'use_batch_norm':args.batch_norm,
                       'dropout_probability':args.dropout_probability,
                       'activation':getattr(F, args.activation_fn),
                       'random_mask':False,
                       'num_bins':args.n_bins,
                       'tails':'linear',
                       'tail_bound':args.tail_bound}

    if args.no_layer_cond:
        def return_CL_net(in_features, out_features):
            """ creates the NN for the checkerboard block """
            net = nflownets.ResidualNet(
                in_features,
                out_features,
                args.hidden_size,
                context_features=cond_label_size,
                num_blocks=args.n_hidden,
                activation=getattr(F, args.activation_fn),
                dropout_probability=args.dropout_probability,
                use_batch_norm=args.batch_norm
            )
            return net
        for layer_id in range(args.num_layer+1):
            current_dim = args.input_size[str(layer_id)]
            list_of_dims.append(current_dim)
            flow_blocks = []
            for _ in range(args.n_blocks - 2*args.n_blocks_checker):
                flow_blocks.append(
                    transforms.MaskedPiecewiseRationalQuadraticAutoregressiveTransform(
                        **flow_params_RQS,
                        features=current_dim,
                        context_features=cond_label_size))
                flow_blocks.append(transforms.RandomPermutation(current_dim))

            checker_mask = np.ones(current_dim)
            checker_mask[::2] *= -1

            for i in range(2*args.n_blocks_checker):
                flow_blocks.append(transforms.PiecewiseRationalQuadraticCouplingTransform(
                    checker_mask, return_CL_net,
                    num_bins=args.n_bins, tails='linear', tail_bound=args.tail_bound))
                checker_mask *= -1
            flow_transforms.append(transforms.CompositeTransform(flow_blocks))
    else:
        current_context = cond_label_size

        for layer_id in range(args.num_layer+1):
            def return_CL_net(in_features, out_features):
                """ creates the NN for the checkerboard block """
                net = nflownets.ResidualNet(
                    in_features,
                    out_features,
                    args.hidden_size,
                    context_features=current_context,
                    num_blocks=args.n_hidden,
                    activation=getattr(F, args.activation_fn),
                    dropout_probability=args.dropout_probability,
                    use_batch_norm=args.batch_norm
                )
                return net

            current_dim = args.input_size[str(layer_id)]
            list_of_dims.append(current_dim)

            # defining the flow (note the order: data->defined flow->base_distribution)
            flow_blocks = []
            for _ in range(args.n_blocks - 2*args.n_blocks_checker):
                flow_blocks.append(
                    transforms.MaskedPiecewiseRationalQuadraticAutoregressiveTransform(
                        **flow_params_RQS,
                        features=current_dim,
                        context_features=current_context))
                torch.nn.init.zeros_(flow_blocks[-1].autoregressive_net.final_layer.weight)
                torch.nn.init.constant_(flow_blocks[-1].autoregressive_net.final_layer.bias,
                                        np.log(np.exp(1-1e-3)-1))

                flow_blocks.append(transforms.RandomPermutation(current_dim))

            checker_mask = np.ones(current_dim)
            checker_mask[::2] *= -1

            for i in range(2*args.n_blocks_checker):
                flow_blocks.append(transforms.PiecewiseRationalQuadraticCouplingTransform(
                    checker_mask, return_CL_net,
                    num_bins=args.n_bins, tails='linear', tail_bound=args.tail_bound))
                checker_mask *= -1
            flow_transforms.append(transforms.CompositeTransform(flow_blocks))
            current_context += current_dim

    args.dim_split = list_of_dims
    if args.no_layer_cond:
        flow_transform = MultiStepTransformNoCond(args.dim_split, flow_transforms)
    else:
        flow_transform = MultiStepTransform(args.dim_split, flow_transforms)

    base_dim = np.array(args.dim_split).cumsum()
    if args.cond_base:
        flow_base_distribution = distributions.ConditionalDiagonalNormal(
            shape=[base_dim[args.num_layer]],
            context_encoder=BaseContext(
                cond_label_size, base_dim[args.num_layer]))
    else:
        flow_base_distribution = distributions.StandardNormal(shape=[base_dim[args.num_layer]])
    flow = flows.Flow(transform=flow_transform, distribution=flow_base_distribution)

    model = flow.to(args.device)
    optimizer = torch.optim.AdamW(model.parameters(), lr=args.lr, weight_decay=0.)

    if args.restore_file:
        # load model and optimizer states
        state = torch.load(args.restore_file, map_location=args.device)
        model.load_state_dict(state['model_state'])
        optimizer.load_state_dict(state['optimizer_state'])
        #args.start_epoch = state['epoch'] + 1
        # set up paths
        args.output_dir = os.path.dirname(args.restore_file)
        print('Loaded settings and model:')
    args.results_file = os.path.join(args.output_dir, args.results_file)

    print(model)
    print("Using {}".format(args.device), file=open(args.results_file, 'a'))
    print(model, file=open(args.results_file, 'a'))

    num_parameters = sum(p.numel() for p in model.parameters() if p.requires_grad)
    output_str = "Having a total of {} parameters"
    print(output_str.format(int(num_parameters)))

    if args.plot_true:
        for x in train_dataloader:
            sorted_order = torch.argsort(x['energy'].squeeze())

            for layer in [0, 1, 2]: #eplus_target_layer_0_log
                sample = x['layer_'+str(layer)]
                sample = ((torch.sigmoid(sample) - ALPHA) / (1. - 2.*ALPHA))*1e5
                filename = args.particle_type + '_target_layer_'+str(layer)+'_log.png'
                plot_calo.plot_calo_batch(sample[sorted_order],
                                          os.path.join(args.output_dir, filename),
                                          layer, ncol=10)
            print("Energies: ", x['energy'][sorted_order]*100.)
            print(x['energy'][sorted_order].size())
            print("plotting target sample done")
            break

    if args.plot_true_avg:
        average_dataloader = get_dataloader(args.particle_type, args.data_dir,
                                            full=True,
                                            apply_logit=True,
                                            device=args.device,
                                            batch_size=100000)

        for x in average_dataloader:
            data = x['layer_0']
            break
        data = ((torch.sigmoid(data) - ALPHA) / (1. - 2.*ALPHA))*1e5
        filename = args.particle_type + '_target_avg.png'
        plot_calo.plot_average_voxel(data,
                                     os.path.join(args.output_dir, filename),
                                     0, vmin=None)
        print("plotting target average done")

        # testing energy_per_layer plot:
        filename = args.particle_type + '_target_E_layer_0.png'
        plot_calo.plot_layer_energy(data,
                                    os.path.join(args.output_dir, filename),
                                    0)

    if args.train:
        train_and_evaluate(model, train_dataloader, test_dataloader, optimizer, args)

    if args.generate:
        # add energy parser, n_sample parser
        generate(model, args, energies=None, n_col=10, include_average=True)
