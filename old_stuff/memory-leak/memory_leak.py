import torch
from time import sleep

def get_a_v(num):
     v = torch.randn(num,1)
     a = torch.sort(torch.randn(num, 16), dim=1)[0]
     return a, v
def searchsorted(bin_locations, inputs, eps=1e-6):
    bin_locations[..., -1] += eps
    return torch.sum(inputs[..., None] >= bin_locations, dim=-1) - 1
num_pts = 5000
a, v = get_a_v(num_pts)

torch_result = (torch.searchsorted(a, v.T.repeat(num_pts,1))-1).T

sleep(5.)

their_result = searchsorted(a, v)
