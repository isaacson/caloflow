"""
CaloFlow: Normalizing Flows for Calorimeter Shower generation
code based on https://github.com/bayesiains/nflows and https://arxiv.org/pdf/1906.04032.pdf

train one flow per layer and condition on previous layers
"""

import argparse
import os

import torch
import torch.nn.functional as F
import numpy as np
from nflows import transforms, distributions, flows
from nflows.nn import nets as nflownets

import plot_calo
from data import get_dataloader
from base_dist import ConditionalDiagonalHalfNormal

torch.set_default_dtype(torch.float64)

parser = argparse.ArgumentParser()

# usage modes
parser.add_argument('--train', action='store_true', help='train a flow')
parser.add_argument('--generate', action='store_true', help='generate from a trained flow')

parser.add_argument('--plot_true', action='store_true',
                    help='plot a random batch of train data ordered by energy')
parser.add_argument('--plot_true_avg', action='store_true',
                    help='plot average of all training data')
parser.add_argument('--no_cuda', action='store_true', help='Do not use cuda.')
parser.add_argument('--restore_file', type=str, help='Path to model to restore.')
parser.add_argument('--output_dir', default='./results', help='Where to store the output')
parser.add_argument('--data_dir', default='/media/claudius/8491-9E93/ML_sources/CaloGAN')
parser.add_argument('--results_file', default='results.txt',
                    help='Filename where to store settings and test results.')
parser.add_argument('--which_cuda', default=0, type=int,
                    help='Which cuda device to use')
parser.add_argument('--with_noise', action='store_true',
                    help='Add 1e-8 noise to dataset')


# CALO specific
parser.add_argument('--particle_type', '-p',
                    help='Which particle to shower, gamma, eplus, or piplus')
parser.add_argument('--num_layer', default=2, type=int,
                    help='How many Calolayers are trained')
parser.add_argument('--energy_encoding', default='direct',
                    help='How the energy conditional is given to the NN: "direct", '+\
                    '"one_hot" or "one_blob"')
parser.add_argument('--energy_encoding_bins', default=100,
                    help='Number of bins in one_hot or one_blob energy encoding')
parser.add_argument('--layer_condition', default='energy', type=str,
                    help='How to condition a layer on previous layer. '+\
                    'Must be one of ["None", "energy", "full"]')
parser.add_argument('--use_full', '-u', action='store_true',
                    help='Train on full dataset, no test set')

# MAF parameters
parser.add_argument('--n_blocks', type=int, default=8,
                    help='Total number of blocks to stack in a model (MADE in MAF).')
parser.add_argument('--hidden_size', type=int, default=1512,
                    help='Hidden layer size for each MADE block in an MAF.')
parser.add_argument('--hidden_size_multiplier', type=int, default=None,
                    help='Hidden layer size for each MADE block in an MAF'+\
                    ' is given by dimension times this factor.')
parser.add_argument('--n_hidden', type=int, default=1,
                    help='Number of hidden layers in each MADE.')
parser.add_argument('--activation_fn', type=str, default='relu',
                    help='What activation function of torch.nn.functional to use in the MADEs.')
parser.add_argument('--batch_norm', action='store_true', default=False,
                    help='Use batch normalization')
parser.add_argument('--n_bins', type=int, default=8,
                    help='Number of bins if piecewise transforms are used')
parser.add_argument('--use_residual', action='store_true', default=False,
                    help='Use residual layers in the NNs')
parser.add_argument('--dropout_probability', '-d', type=float, default=0.,
                    help='dropout probability')
parser.add_argument('--tail_bound', type=float, default=14., help='Domain of the RQS')
parser.add_argument('--cond_base', action='store_true',
                    help='Use Gaussians conditioned on energy as base distribution.')
parser.add_argument('--cond_half_base', action='store_true',
                    help='Use Half-Gaussians conditioned on energy as base distribution.')

# training params
parser.add_argument('--batch_size', type=int, default=200)
parser.add_argument('--n_epochs', type=int, default=50)
parser.add_argument('--lr', type=float, default=1e-4, help='Initial Learning rate.')
parser.add_argument('--log_interval', type=int, default=175,
                    help='How often to show loss statistics and save samples.')

# used in transformation between energy and logit space:
# (should match the ALPHA in data.py)
ALPHA = 1e-6

def logit(x):
    return torch.log(x / (1.0 - x))

def logit_trafo(x):
    local_x = ALPHA + (1. - 2.*ALPHA) * x
    return logit(local_x)

def inverse_logit(x):
    return ((torch.sigmoid(x) - ALPHA) / (1. - 2.*ALPHA))

def one_hot(values, num_bins):
    """ one-hot encoding of values into num_bins """
    # values are energies in [0, 1], need to be converted to integers in [0, num_bins-1]
    values *= num_bins
    values = values.type(torch.long)
    ret = F.one_hot(values, num_bins)
    return ret.squeeze().double()

def one_blob(values, num_bins):
    """ one-blob encoding of values into num_bins, cf sec. 4.3 of 1808.03856 """
    # torch.tile() not yet in stable release, use numpy instead
    values = values.cpu().numpy()[..., np.newaxis]
    y = np.tile(((0.5/num_bins) + np.arange(0., 1., step=1./num_bins)), values.shape)
    res = np.exp(((-num_bins*num_bins)/2.)
                 * (y-values)**2)
    res = np.reshape(res, (-1, values.shape[-1]*num_bins))
    return torch.tensor(res)

def remove_nans(tensor):
    """removes elements in the given batch that contain nans
       returns the new tensor and the number of removed elements"""
    tensor_flat = tensor.flatten(start_dim=1)
    good_entries = torch.all(tensor_flat==tensor_flat, axis=1)
    res_flat = tensor_flat[good_entries]
    tensor_shape = list(tensor.size())
    tensor_shape[0] = -1
    res = res_flat.reshape(tensor_shape)
    return res, len(tensor) - len(res)

@torch.no_grad()
def generate_from_flow(model_list, args, num_pts, context):
    """ generate images from 3 flows """
    sample0 = model_list[0].sample(num_pts, context)

    if args.layer_condition == 'None':
        pass
    elif args.layer_condition == 'energy':
        previous_layer = inverse_logit(sample0).squeeze()
        previous_energy = previous_layer.sum(dim=1, keepdims=True)
        if args.energy_encoding == 'direct':
            pass
        elif args.energy_encoding == 'one_hot':
            previous_energy = one_hot(previous_energy, args.energy_encoding_bins)
        elif args.energy_encoding == 'one_blob':
            previous_energy = one_blob(previous_energy, args.energy_encoding_bins).to(args.device)
        context = torch.cat((context, previous_energy), 1)
    elif args.layer_condition == 'full':
        previous_energy = torch.log10(inverse_logit(sample0.squeeze())+2e-7)+3.75
        context = torch.cat((context, previous_energy), 1)

    sample1 = model_list[1].sample(num_pts, context)

    if args.layer_condition == 'None':
        pass
    elif args.layer_condition == 'energy':
        previous_layer1 = inverse_logit(sample1).squeeze()
        previous_energy1 = previous_layer1.sum(dim=1, keepdims=True)
        if args.energy_encoding == 'direct':
            pass
        elif args.energy_encoding == 'one_hot':
            previous_energy1 = one_hot(previous_energy1, args.energy_encoding_bins)
        elif args.energy_encoding == 'one_blob':
            previous_energy1 = one_blob(previous_energy1, args.energy_encoding_bins).to(args.device)
        context = torch.cat((context, previous_energy1), 1)
    elif args.layer_condition == 'full':
        previous_energy1 = torch.log10(inverse_logit(sample1.squeeze())+2e-7)+3.75
        context = torch.cat((context, previous_energy1), 1)

    sample2 = model_list[2].sample(num_pts, context)

    return torch.cat((sample0, sample1, sample2), 2)

@torch.no_grad()
def generate(model, args, energies=None, n_col=10, step=None, include_average=False):
    """ generate samples from the trained model"""
    for model_layer in model:
        model_layer.eval()

    if energies is None:
        #energies = torch.tensor([10., 20., 30., 40., 50., 60., 70., 80., 90.]) / 100.
        energies = torch.arange(0.01, 1.01, 0.01)
        energies[-1] -= 1e-6
    if args.energy_encoding == 'direct':
        energies = torch.reshape(energies, (-1, 1)).to(args.device)
    elif args.energy_encoding == 'one_hot':
        energies = one_hot(energies, args.energy_encoding_bins).to(args.device)
    elif args.energy_encoding == 'one_blob':
        energies = one_blob(energies, args.energy_encoding_bins).to(args.device)

    #samples = model.sample(1, energies)
    samples = generate_from_flow(model, args, 1, energies)
    sample_list = [*torch.split(samples, args.dim_split, dim=-1)]

    for plot_layer_id, sample in enumerate(sample_list):
        # reshape and transform back to energy
        sample = sample.view(-1, *args.input_dims[str(plot_layer_id)])

        sample = ((torch.sigmoid(sample) - ALPHA) / (1. - 2.*ALPHA))*1e5
        filename = 'generated_samples_layer_'+str(plot_layer_id) +\
            (step is not None)*'_epoch_{}'.format(step) + '.png'

        plot_calo.plot_calo_batch(sample, os.path.join(args.output_dir, filename),
                                  plot_layer_id, ncol=n_col)
    if include_average and ((step % 5 == 1) or step == args.n_epochs):
        num_pts = 10000
        energies = 0.99*torch.rand((num_pts,)) + 0.01
        if args.energy_encoding == 'direct':
            energies = torch.reshape(energies, (-1, 1)).to(args.device)
        elif args.energy_encoding == 'one_hot':
            energies = one_hot(energies, args.energy_encoding_bins).to(args.device)
        elif args.energy_encoding == 'one_blob':
            energies = one_blob(energies, args.energy_encoding_bins).to(args.device)

        #samples = model.sample(1, energies)
        samples = generate_from_flow(model, args, 1, energies)
        sample_list = [*torch.split(samples, args.dim_split, dim=-1)]

        for plot_layer_id, sample in enumerate(sample_list):
            sample = sample.view(num_pts, *args.input_dims[str(plot_layer_id)])
            sample, num_bad = remove_nans(sample)

            if not num_bad == num_pts:
                print("Removed {} samples of {} from average/distribution plots".format(num_bad,
                                                                                        num_pts))
                print(sample.to('cpu').detach().numpy().min(),
                      sample.to('cpu').detach().numpy().max())
                sample = ((torch.sigmoid(sample) - ALPHA) / (1. - 2.*ALPHA))*1e5
                print(sample.to('cpu').detach().numpy().min(),
                      sample.to('cpu').detach().numpy().max())
                filename = 'generated_samples_avg_layer_'+str(plot_layer_id) +\
                    (step is not None)*'_epoch_{}'.format(step) + '.png'
                plot_calo.plot_average_voxel(sample,
                                             os.path.join(args.output_dir, filename),
                                             plot_layer_id, vmin=None)

                filename = 'generated_samples_E_layer_'+str(plot_layer_id) +\
                    (step is not None)*'_epoch_{}'.format(step) + '.png'
                plot_calo.plot_layer_energy(sample,
                                            os.path.join(args.output_dir, filename),
                                            plot_layer_id, plot_ref=args.particle_type,
                                            epoch_nr=step)
                filename = 'sparsity_1e-3_layer_'+str(plot_layer_id) +\
                           (step is not None)*'_epoch_{}'.format(step) + '.png'
                plot_calo.plot_layer_sparsity(sample,
                                              os.path.join(args.output_dir, filename),
                                              plot_layer_id, plot_ref=args.particle_type,
                                              epoch_nr=step, threshold=1e-3)
                filename = 'sparsity_1e-2_layer_'+str(plot_layer_id) +\
                           (step is not None)*'_epoch_{}'.format(step) + '.png'
                plot_calo.plot_layer_sparsity(sample,
                                              os.path.join(args.output_dir, filename),
                                              plot_layer_id, plot_ref=args.particle_type,
                                              epoch_nr=step, threshold=1e-2)
                filename = 'sparsity_0_layer_'+str(plot_layer_id) +\
                           (step is not None)*'_epoch_{}'.format(step) + '.png'
                plot_calo.plot_layer_sparsity(sample,
                                              os.path.join(args.output_dir, filename),
                                              plot_layer_id, plot_ref=args.particle_type,
                                              epoch_nr=step, threshold=0.)
            else:
                print("Skipping plotting due to bad sample")

        if args.num_layer == 2:
            filename = 'generated_samples_E_total' +\
                       (step is not None)*'_epoch_{}'.format(step) + '.png'

            samples = ((torch.sigmoid(samples) - ALPHA) / (1. - 2.*ALPHA))*1e5

            plot_calo.plot_total_energy(samples,
                                        os.path.join(args.output_dir, filename),
                                        plot_ref=args.particle_type,
                                        epoch_nr=step)
            for layer in [0, 1, 2]:
                for use_log in [False, True]:
                    filename = 'energy_fraction_layer_'+str(layer)+\
                               (use_log)*'_log'+\
                               (step is not None)*'_epoch_{}'.format(step) + '.png'
                    plot_calo.plot_energy_fraction(samples,
                                                   os.path.join(args.output_dir, filename),
                                                   layer, plot_ref=args.particle_type,
                                                   epoch_nr=step, use_log=use_log)

@torch.no_grad()
def evaluate_cond(model, dataloader, epoch, args, num_pts):
    """ Evaluate the model, integrating over the conditional using num_pts points"""
    raise NotImplementedError()
    model.eval()
    # assume flat prior
    logprior = torch.tensor(1./num_pts).log().to(args.device)
    loglike = [[] for _ in range(num_pts)]
    energies = torch.arange(0., 1., 1./num_pts) + 0.5/num_pts

    for i in range(num_pts):
        if args.energy_encoding == 'direct':
            energy = torch.reshape(energies[i], (-1, 1))
        elif args.energy_encoding == 'one_hot':
            energy = one_hot(energies[i], args.energy_encoding_bins)
        elif args.energy_encoding == 'one_blob':
            energy = one_blob(energies[i], args.energy_encoding_bins)
        for data in dataloader:
            x0 = data['layer_0']
            x1 = data['layer_1']
            x2 = data['layer_2']
            local_energy = energy.repeat((x0.shape[0], 1)).to(args.device)
            if args.num_layer == 0:
                del x1, x2
                x = x0.view(x0.shape[0], -1).to(args.device)
            elif args.num_layer == 1:
                del x2
                layer0 = x0.view(x0.shape[0], -1)
                layer1 = x1.view(x1.shape[0], -1)
                x = torch.cat((layer0, layer1), 1).to(args.device)
            elif args.num_layer == 2:
                layer0 = x0.view(x0.shape[0], -1)
                layer1 = x1.view(x1.shape[0], -1)
                layer2 = x2.view(x2.shape[0], -1)
                x = torch.cat((layer0, layer1, layer2), 1).to(args.device)

            loglike[i].append(model.log_prob(x, local_energy))

        loglike[i] = torch.cat(loglike[i], dim=0)
        print("Evaluation done to {}%".format(100.*(i+1)/num_pts), end='\r')
    loglike = torch.stack(loglike, dim=1)
    logprobs = logprior + loglike.logsumexp(dim=1)

    logprob_mean = logprobs.mean(0)
    logprob_std = logprobs.var(0).sqrt() / np.sqrt(len(dataloader.dataset))

    output = 'Cond. Evaluate ' + (epoch is not None)*'(epoch {}) -- '.format(epoch+1) +\
        'logp(x) = {:.3f} +/- {:.3f}'

    print(output.format(logprob_mean, logprob_std))
    print(output.format(logprob_mean, logprob_std), file=open(args.results_file, 'a'))
    return logprob_mean, logprob_std

@torch.no_grad()
def evaluate(model, dataloader, epoch, args):
    """Evaluate the model, i.e find the mean log_prob of the test set
       Energy is taken to be the energy of the image, so no
       marginalization is performed.
    """
    for layer_model in model:
        layer_model.eval()
    loglike = []
    for data in dataloader:

        x0 = data['layer_0']
        x1 = data['layer_1']
        x2 = data['layer_2']
        if args.energy_encoding == 'direct':
            energy = data['energy']
        elif args.energy_encoding == 'one_hot':
            energy = one_hot(data['energy'], args.energy_encoding_bins)
        elif args.energy_encoding == 'one_blob':
            energy = one_blob(data['energy'], args.energy_encoding_bins)

        y = energy.to(args.device)
        if args.num_layer == 0:
            raise NotImplementedError
            del x1, x2
            x = x0.view(x0.shape[0], -1).to(args.device)
        elif args.num_layer == 1:
            raise NotImplementedError
            del x2
            layer0 = x0.view(x0.shape[0], -1)
            layer1 = x1.view(x1.shape[0], -1)
            x = torch.cat((layer0, layer1), 1).to(args.device)
        elif args.num_layer == 2:
            layer0 = x0.view(x0.shape[0], -1).to(args.device)
            layer1 = x1.view(x1.shape[0], -1).to(args.device)
            layer2 = x2.view(x2.shape[0], -1).to(args.device)
            #x = torch.cat((layer0, layer1, layer2), 1).to(args.device)

        current_logprob = model[0].log_prob(layer0, y)

        if args.layer_condition == 'None':
            pass
        elif args.layer_condition == 'energy':
            previous_energy = inverse_logit(layer0)
            previous_energy = previous_energy.sum(dim=1, keepdims=True)
            if args.energy_encoding == 'direct':
                pass
            elif args.energy_encoding == 'one_hot':
                previous_energy = one_hot(previous_energy, args.energy_encoding_bins)
            elif args.energy_encoding == 'one_blob':
                previous_energy = one_blob(previous_energy, args.energy_encoding_bins).to(args.device)
            y = torch.cat((y, previous_energy), 1)
        elif args.layer_condition == 'full':
            previous_energy = torch.log10(inverse_logit(layer0)+2e-7)+3.75
            y = torch.cat((y, previous_energy), 1)

        current_logprob += model[1].log_prob(layer1, y)

        if args.layer_condition == 'None':
            pass
        elif args.layer_condition == 'energy':
            previous_energy1 = inverse_logit(layer1)
            previous_energy1 = previous_energy1.sum(dim=1, keepdims=True)
            if args.energy_encoding == 'direct':
                pass
            elif args.energy_encoding == 'one_hot':
                previous_energy1 = one_hot(previous_energy1, args.energy_encoding_bins)
            elif args.energy_encoding == 'one_blob':
                previous_energy1 = one_blob(previous_energy1, args.energy_encoding_bins).to(args.device)
            y = torch.cat((y, previous_energy1), 1)
        elif args.layer_condition == 'full':
            previous_energy1 = torch.log10(inverse_logit(layer1)+2e-7)+3.75
            y = torch.cat((y, previous_energy1), 1)

        current_logprob += model[2].log_prob(layer2, y)

        loglike.append(current_logprob)

    logprobs = torch.cat(loglike, dim=0).to(args.device)

    logprob_mean = logprobs.mean(0)
    logprob_std = logprobs.var(0).sqrt() / np.sqrt(len(dataloader.dataset))

    output = 'Evaluate ' + (epoch is not None)*'(epoch {}) -- '.format(epoch+1) +\
        'logp(x, at E(x)) = {:.3f} +/- {:.3f}'

    print(output.format(logprob_mean, logprob_std))
    print(output.format(logprob_mean, logprob_std), file=open(args.results_file, 'a'))
    return logprob_mean, logprob_std

def train(model, dataloader, optimizer, epoch, args):

    for layer_model in model:
        layer_model.train()
    for i, data in enumerate(dataloader):

        x0 = data['layer_0']
        x1 = data['layer_1']
        x2 = data['layer_2']
        if args.energy_encoding == 'direct':
            energy = data['energy']
        elif args.energy_encoding == 'one_hot':
            energy = one_hot(data['energy'], args.energy_encoding_bins)
        elif args.energy_encoding == 'one_blob':
            energy = one_blob(data['energy'], args.energy_encoding_bins)

        y = energy.to(args.device)
        if args.num_layer == 0:
            raise NotImplementedError
            del x1, x2
            x = x0.view(x0.shape[0], -1).to(args.device)
        elif args.num_layer == 1:
            raise NotImplementedError
            del x2
            layer0 = x0.view(x0.shape[0], -1)
            layer1 = x1.view(x1.shape[0], -1)
            x = torch.cat((layer0, layer1), 1).to(args.device)
        elif args.num_layer == 2:
            layer0 = x0.view(x0.shape[0], -1).to(args.device)
            layer1 = x1.view(x1.shape[0], -1).to(args.device)
            layer2 = x2.view(x2.shape[0], -1).to(args.device)
            # x = torch.cat((layer0, layer1, layer2), 1).to(args.device)

        loss0 = - model[0].log_prob(layer0, y).mean(0)

        optimizer[0].zero_grad()
        loss0.backward()
        optimizer[0].step()

        if args.layer_condition == 'None':
            pass
        elif args.layer_condition == 'energy':
            previous_energy = inverse_logit(layer0)
            previous_energy = previous_energy.sum(dim=1, keepdims=True)
            if args.energy_encoding == 'direct':
                pass
            elif args.energy_encoding == 'one_hot':
                previous_energy = one_hot(previous_energy, args.energy_encoding_bins)
            elif args.energy_encoding == 'one_blob':
                previous_energy = one_blob(previous_energy, args.energy_encoding_bins).to(args.device)
            y = torch.cat((y, previous_energy), 1)
        elif args.layer_condition == 'full':
            previous_energy = torch.log10(inverse_logit(layer0)+2e-7)+3.75
            y = torch.cat((y, previous_energy), 1)

        loss1 = - model[1].log_prob(layer1, y).mean(0)
        optimizer[1].zero_grad()
        loss1.backward()
        optimizer[1].step()

        if args.layer_condition == 'None':
            pass
        elif args.layer_condition == 'energy':
            previous_energy1 = inverse_logit(layer1)
            previous_energy1 = previous_energy1.sum(dim=1, keepdims=True)
            if args.energy_encoding == 'direct':
                pass
            elif args.energy_encoding == 'one_hot':
                previous_energy1 = one_hot(previous_energy1, args.energy_encoding_bins)
            elif args.energy_encoding == 'one_blob':
                previous_energy1 = one_blob(previous_energy1, args.energy_encoding_bins).to(args.device)
            y = torch.cat((y, previous_energy1), 1)
        elif args.layer_condition == 'full':
            previous_energy1 = torch.log10(inverse_logit(layer1)+2e-7)+3.75
            y = torch.cat((y, previous_energy1), 1)

        loss2 = - model[2].log_prob(layer2, y).mean(0)
        optimizer[2].zero_grad()
        loss2.backward()
        optimizer[2].step()

        args.train_loss.append(loss0.tolist()+loss1.tolist()+loss2.tolist())

        if i % args.log_interval == 0:
            print_str = 'epoch {:3d} / {}, step {:4d} / {};'+\
                ' loss0 {:.2f}; loss1 {:.2f}; loss2 {:.2f}; loss_total {:.2f}'
            print(print_str.format(epoch+1, args.n_epochs, i, len(dataloader),
                                   loss0.item(), loss1.item(), loss2.item(),
                                   args.train_loss[-1]))
            print(print_str.format(epoch+1, args.n_epochs, i, len(dataloader),
                                   loss0.item(), loss1.item(), loss2.item(),
                                   args.train_loss[-1]),
                  file=open(args.results_file, 'a'))

def train_and_evaluate(model, train_loader, test_loader, optimizer, args):
    """As the name says, train the flow and evaluate after each epoch
       TO-DO: implement learning rate schedule
    """
    best_eval_logprob = float('-inf')
    lr_schedules = []
    for optim in optimizer:
        lr_schedules.append(torch.optim.lr_scheduler.MultiStepLR(optim,
                                                                 milestones=[15, 40, 70, 100, 125],
                                                                 gamma=0.5,
                                                                 verbose=True))
    for i in range(args.n_epochs):
        train(model, train_loader, optimizer, i, args)
        #eval_logprob, _ = evaluate_cond(model, test_loader, i, args, 20)
        with torch.no_grad():
            eval_logprob, _ = evaluate(model, test_loader, i, args)
            args.test_loss.append(-eval_logprob.to('cpu').numpy())

        # save training checkpoint
        #torch.save({'epoch': i,
        #            'model_state': model.state_dict(),
        #            'optimizer_state': optimizer.state_dict()},
        #           os.path.join(args.output_dir, 'model_checkpoint.pt'))
        # save model only
        #torch.save(model.state_dict(), os.path.join(args.output_dir, 'model_state.pt'))

        # save best state
        #if eval_logprob > best_eval_logprob:
        #    best_eval_logprob = eval_logprob
        #    torch.save({'epoch': i,
        #                'model_state': model.state_dict(),
        #                'optimizer_state': optimizer.state_dict()},
        #               os.path.join(args.output_dir, 'best_model_checkpoint.pt'))

        # plot sample
        with torch.no_grad():
            generate(model, args, step=i+1, include_average=True)
        for lr_schedule in lr_schedules:
            lr_schedule.step()
        plot_calo.plot_loss(args.train_loss, args.test_loss,
                            os.path.join(args.output_dir, 'loss.png'))
    #with torch.no_grad():
    #    eval_logprob, _ = evaluate_cond(model, test_loader, i, args, 25)

class BaseContext(torch.nn.Module):
    """ Small NN to map energy input to mean and width of base_gaussians"""
    def __init__(self, context_size, dimensionality):
        """ context_size: length of context vector
            dimensionality: number of dimensions of base dist.
        """
        super(BaseContext, self).__init__()
        self.layer1 = torch.nn.Linear(context_size, dimensionality)
        self.layer2 = torch.nn.Linear(dimensionality, dimensionality)
        self.output = torch.nn.Linear(dimensionality, 2*dimensionality)

    def forward(self, x):
        x = F.relu(self.layer1(x))
        x = F.relu(self.layer2(x))
        out = self.output(x)
        return out

class BaseHalfContext(torch.nn.Module):
    """ Small NN to map energy input to width of base half gaussians"""
    def __init__(self, context_size, dimensionality):
        """ context_size: length of context vector
            dimensionality: number of dimensions of base dist.
        """
        super(BaseHalfContext, self).__init__()
        self.layer1 = torch.nn.Linear(context_size, dimensionality)
        self.layer2 = torch.nn.Linear(dimensionality, dimensionality)
        self.output = torch.nn.Linear(dimensionality, dimensionality)

    def forward(self, x):
        x = F.relu(self.layer1(x))
        x = F.relu(self.layer2(x))
        out = self.output(x)
        return out

if __name__ == '__main__':
    args = parser.parse_args()

    assert args.num_layer in [0, 1, 2], (
        "Calorimeter only has layer 0, 1, or 2")

    # check if output_dir exists
    if not os.path.isdir(args.output_dir):
        os.makedirs(args.output_dir)

    args.results_file = os.path.join(args.output_dir, args.results_file)

    # setup device
    args.device = torch.device('cuda:'+str(args.which_cuda) if torch.cuda.is_available() and not args.no_cuda\
                               else 'cpu')
    print("Using {}".format(args.device))
    print("Using {}".format(args.device), file=open(args.results_file, 'a'))

    assert args.particle_type in ['gamma', 'eplus', 'piplus'], \
        'Particle type must be either gamma, eplus, or piplus!'

    if args.plot_true:
        print("setting batch size to 100 to plot pretty target plot")
        args.batch_size = 100

    if args.use_full:
        train_dataloader = get_dataloader(args.particle_type, args.data_dir,
                                          full=True, apply_logit=True,
                                          device=args.device,
                                          batch_size=args.batch_size,
                                          with_noise=args.with_noise)

    else:
        train_dataloader, test_dataloader = get_dataloader(args.particle_type,
                                                           args.data_dir,
                                                           full=False,
                                                           apply_logit=True,
                                                           device=args.device,
                                                           batch_size=args.batch_size,
                                                           with_noise=args.with_noise)
    if args.energy_encoding == 'direct':
        cond_label_size = 1
    elif args.energy_encoding in ['one_hot', 'one_blob']:
        cond_label_size = int(args.energy_encoding_bins)
    else:
        raise ValueError('energy_encoding not in ["direct", "one_hot", "one_blob"]')

    if args.layer_condition not in ["None", "energy", "full"]:
        raise ValueError('layer_condition not in ["None", "energy", "full"]')


    args.input_size = train_dataloader.dataset.input_size
    args.input_dims = train_dataloader.dataset.input_dims
    args.train_loss = []
    args.test_loss = []

    flow_params_RQS = {'num_blocks':args.n_hidden,
                       'use_residual_blocks':args.use_residual,
                       'use_batch_norm':args.batch_norm,
                       'dropout_probability':args.dropout_probability,
                       'activation':getattr(F, args.activation_fn),
                       'random_mask':False,
                       'num_bins':args.n_bins,
                       'tails':'linear',
                       'tail_bound':args.tail_bound,
                       'min_bin_width': 1e-6,
                       'min_bin_height': 1e-6,
                       'min_derivative': 1e-6}

    flow_list = []
    optimizer_list = []
    args.dim_split = []
    args.dim_sum = 0

    for flow_index in range(args.num_layer+1):
        current_dim = args.input_size[str(flow_index)]
        args.dim_split.append(current_dim)

        flow_blocks = []
        if args.layer_condition == 'None':
            condition_dimension = cond_label_size
        elif args.layer_condition == 'energy':
            condition_dimension = cond_label_size * (1 + flow_index)
        elif args.layer_condition == 'full':
            condition_dimension = cond_label_size + args.dim_sum
        args.dim_sum += current_dim

        if args.hidden_size_multiplier is not None:
            hidden_features = args.hidden_size_multiplier*current_dim
        else:
            hidden_features = args.hidden_size

        for _ in range(args.n_blocks):
            flow_blocks.append(
                transforms.MaskedPiecewiseRationalQuadraticAutoregressiveTransform(
                    **flow_params_RQS,
                    context_features=condition_dimension,
                    hidden_features=hidden_features,
                    features=current_dim
                ))
            #torch.nn.init.zeros_(flow_blocks[-1].autoregressive_net.final_layer.weight)
            #torch.nn.init.constant_(flow_blocks[-1].autoregressive_net.final_layer.bias,
            #                            np.log(np.exp(1-1e-3)-1))

            flow_blocks.append(transforms.RandomPermutation(current_dim))

        flow_transform = transforms.CompositeTransform(flow_blocks)

        if args.cond_half_base:
            flow_base_distribution = ConditionalDiagonalHalfNormal(
                shape=[current_dim],
                positions=logit_trafo(torch.zeros(current_dim)).to(args.device),
                context_encoder=BaseHalfContext(
                    condition_dimension, current_dim))
        elif args.cond_base:
            flow_base_distribution = distributions.ConditionalDiagonalNormal(
                shape=[current_dim],
                context_encoder=BaseContext(
                    condition_dimension, current_dim))
        else:
            flow_base_distribution = distributions.StandardNormal(shape=[current_dim])
        flow = flows.Flow(transform=flow_transform, distribution=flow_base_distribution)

        model = flow.to(args.device)
        flow_list.append(model)
        #if flow_index == 2:
        #    optimizer_list.append(torch.optim.Adamax(model.parameters(), lr=args.lr))
        #else:
        #    optimizer_list.append(torch.optim.Adam(model.parameters(), lr=args.lr))

        optimizer_list.append(torch.optim.Adam(model.parameters(), lr=args.lr))
        #optimizer_list.append(torch.optim.SGD(model.parameters(), lr=args.lr))

        print(model)
        print(model, file=open(args.results_file, 'a'))

    #if args.restore_file:
    #    # load model and optimizer states
    #    state = torch.load(args.restore_file, map_location=args.device)
    #    model.load_state_dict(state['model_state'])
    #    optimizer.load_state_dict(state['optimizer_state'])
    #    #args.start_epoch = state['epoch'] + 1
    #    # set up paths
    #    args.output_dir = os.path.dirname(args.restore_file)
    #    print('Loaded settings and model:')


    total_parameters = 0
    for index, model in enumerate(flow_list):
        num_parameters = sum(p.numel() for p in model.parameters() if p.requires_grad)
        total_parameters += num_parameters
        output_str = "Flow {} has a total of {} parameters"
        print(output_str.format(int(index), int(num_parameters)))
        print(output_str.format(int(index), int(num_parameters)),
              file=open(args.results_file, 'a'))
    print("Total setup has {} parameters".format(int(total_parameters)))
    print("Total setup has {} parameters".format(int(total_parameters)),
          file=open(args.results_file, 'a'))

    if args.train:
        train_and_evaluate(flow_list, train_dataloader, test_dataloader, optimizer_list, args)

    if args.generate:
        # add energy parser, n_sample parser
        generate(flow_list, args, energies=None, n_col=10, include_average=True)
