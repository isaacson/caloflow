""" Contains the half-normal base distribution, based on
    nflows.distribution.base.ConditionalDiagonalNormal """

import numpy as np
import torch

from nflows.distributions.base import Distribution
from nflows.utils import torchutils

class ConditionalDiagonalHalfNormal(Distribution):
    """A diagonal multivariate HalfNormal whose width parameters are functions of a context."""

    def __init__(self, shape, positions, context_encoder=None, side='right'):
        """Constructor.
        Args:
            shape: list, tuple or torch.Size, the shape of the input variables.
            positions: list of floats, where to put the cuts
            context_encoder: callable or None, encodes the context to the distribution parameters.
                If None, defaults to the identity function.
            side: str ('left' or 'right') which half to keep
        """
        super().__init__()
        self._shape = torch.Size(shape)
        self._positions = positions
        self._side = side
        assert self._side in ['left', 'right']
        if context_encoder is None:
            self._context_encoder = lambda x: x
        else:
            self._context_encoder = context_encoder
        #assert self._positions.size() == self._shape
        self.register_buffer("_log_z",
                             torch.tensor((0.5 * np.prod(shape) * np.log(2 * np.pi))
                                          - np.log(2.),
                                          dtype=torch.float64),
                             persistent=False)

    def _compute_params(self, context):
        """Compute the means and log stds form the context."""
        if context is None:
            raise ValueError("Context can't be None.")

        params = self._context_encoder(context)
        if params.shape[0] != context.shape[0]:
            raise RuntimeError(
                "The batch dimension of the parameters is inconsistent with the input."
            )

        log_stds = params.reshape(params.shape[0], *self._shape)
        means = self._positions.unsqueeze(0).repeat(params.shape[0], 1)
        return means, log_stds

    def _log_prob(self, inputs, context):
        if inputs.shape[1:] != self._shape:
            raise ValueError(
                "Expected input of shape {}, got {}".format(
                    self._shape, inputs.shape[1:]
                )
            )

        # Compute parameters.
        means, log_stds = self._compute_params(context)

        assert means.shape == inputs.shape and log_stds.shape == inputs.shape

        # Compute log prob.
        norm_inputs = (inputs - means) * torch.exp(-log_stds)
        log_prob = -0.5 * torchutils.sum_except_batch(
            norm_inputs ** 2, num_batch_dims=1
        )
        log_prob -= torchutils.sum_except_batch(log_stds, num_batch_dims=1)
        log_prob -= self._log_z
        if self._side == 'right':
            ret = torch.where(((inputs - self._positions) < 0.).any(dim=1),
                              -torch.ones_like(log_prob)*float('inf'), log_prob)
            #print(((inputs - self._positions) < 0.).any(dim=1).size())
            #print((inputs - self._positions).min(dim=1)[0].size())
            #print(log_prob.size())
            #ret = torch.where(((inputs - self._positions) < 0.).any(dim=1),
            #                  (inputs - self._positions).min(dim=1)[0]*1e3, log_prob)
        else:
            ret = torch.where(((inputs - self._positions) > 0.).any(dim=1),
                              -torch.ones_like(log_prob)*float('inf'), log_prob)
        return ret

    def _sample(self, num_samples, context):
        # Compute parameters.
        means, log_stds = self._compute_params(context)
        stds = torch.exp(log_stds)
        means = torchutils.repeat_rows(means, num_samples)
        stds = torchutils.repeat_rows(stds, num_samples)

        # Generate samples.
        context_size = context.shape[0]
        noise = torch.abs(torch.randn(context_size * num_samples, *
                                      self._shape, device=means.device))
        if self._side == 'left':
            noise *= -1.
        samples = means + stds * noise
        return torchutils.split_leading_dim(samples, [context_size, num_samples])

    def _mean(self, context):
        means, _ = self._compute_params(context)
        return means
