# CaloFlow: Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows
## by Claudius Krause and David Shih

This repository contains the source code for reproducing the results of 
_"CaloFlow: Fast and Accurate Generation of Calorimeter Showers with Normalizing Flows"_ by Claudius Krause and David Shih. If you use the code, please cite [arXiv:2106.05285](https://arxiv.org/abs/2106.05285)


### Detector Layout and Training Data
We consider the same toy detector that the original [CaloGAN](https://arxiv.org/abs/1712.10321) used. It is a three-layer, liquid Argon (LAr) electromagnetic calorimeter (ECal) cube with 480mm side length. The three layers have a voxel resolution of $`3\times 96`$, $`12\times 12`$, and $`12\times 6`$, respectively. 

We use the same GEANT4-based training data as [CaloGAN](https://arxiv.org/abs/1712.10321), it can be found at [![DOI](https://zenodo.org/badge/DOI/10.17632/pvn3xc3wy5.1.svg)](https://doi.org/10.17632/pvn3xc3wy5.1). To speed up the plotting of reference data, we store the relavant features in separate .hdf files ("plots_reference_xx.hdf", with xx the applied threshold in MeV). We obtained the CaloGAN reference by training our own GAN based on the source code available at [![DOI](https://zenodo.org/badge/82329392.svg)](https://zenodo.org/badge/latestdoi/82329392). 

Running `python split_hdf5.py --file=eplus.hdf5` from the folder hdf5-helper/ splits the original hdf5 file in two files with a training and testing set. 

### Running CaloFlow
<!--# file descriptions, command to run like paper resutls, addtional contained options -->
CaloFlow uses the [pytorch](https://pytorch.org/) implementation of Normalizing Flows called [nflows](https://github.com/bayesiains/nflows). 

To train CaloFlow as in the paper, run 
`python run.py -p=eplus --train --data_dir=path/to/GEANT/data/ --output_dir=./results/ --with_noise`

To generate a sample of 100k events to file, run 
`python run.py -p=eplus --generate_to_file --restore_file=./path/to/weights.pt`

Samples for gamma and piplus can be generated with the options `-p=gamma` and `-p=piplus` respectively. One can also specify to evaluate the trained flow on the test set with `--evaluate` and to generate a sample that is only plotted and not saved in a separate file using `--generate`. For loading a saved flow, add `--restore_file=path/to/weights.pt` to the call. Per default, the full model with all weights, but also with the full optimizer state is saved during training. Calling `run.py` with the option `--save_only_weights` loads such a saved model and saves only the weights and not the optimizer state. 

Additional options that were used in development but not the final version (there is no guarantee that they still work properly) are:
- `--mode`: Which flow setup to use in general. Options are 
  * `single_recursive`: Default. Use a 2-flow setup where the first flow recursively learns the energy deposit in each layer conditioned on the total energy. The second flow learns the shower-shapes of all three, normalized, calorimeter layers. 
  * `single`: A single flow is used to learn the full 504-dimensional sample space. 
  * `three_recursive`: A 2-step setup with four flows in total. The first flow recursively learns the energy deposit in each layer conditioned on the total energy. The remaining three flows learn the shower shape of each calorimeter layer separately. The option `--layer_condition` sets how the flow of calorimeter layer $`k`$ is conditioned on the information of layer(s) $`j<k`$. Options are `["None", "energy", "full", "NN"]`
  * `--normed` normalizes the total energy deposit in all three layers combined to 1. This should only work for `--mode=single` or `--mode=three`. 
- `--energy_encoding`: How the conditional information (i.e. the energy) is fed into the networks. Options are 
    * `direct`: Uses a single number, no transformation is applied.
    * `logdirect`: Default. Uses a single number, the logarithm of the energy. 
    * `one_hot`: The condition is one-hot encoded using `--energy_encoding_bins` bins (default=100).
    * `one_blob`: The condition is one-blob encoded using `--energy_encoding_bins` bins (default=100).
- `--no_cuda` and `--which_cuda` allows the user to either not use the GPU or to specify on which of the possibly multiple available GPUs to run. 
- `--threshold`: Sets the threshold (in MeV) that is applied to the sample after generation as part of the post-processing. 

### Running the Classifier

First, a dataset file containing both the GEANT4 and sampled data must be created. This can be done with the script [merge_hdf5.py](hdf5-helper/merge_hdf5.py) and the command 
`pyhton merge_hdf5.py --file1=file_with_truth_label_0.hdf5 -- file2=file_with_truth_label_1.hdf5`. 

Then run 

`python classify.py -p=filename_of_merged_dataset_without_extension --data_dir=path/to/GEANT/data/ --save_dir=./classifier/  --mode=[DNN or CNN]`

to start training the classifier. Normalized calorimeter layer data can be used with `--normalize` and `--use_logit` transforms data into logit space before training. 
